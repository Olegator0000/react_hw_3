import React, { useState, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import CartPage from './components/CartPage';
import FavoritesPage from './components/FavoritesPage';
import Header from './components/header/header';
import Navbar from './components/Navbar';
import './App.css';

const useLocalStorage = (key, defaultValue) => {
    const [value, setValue] = useState(() => {
        try {
            const storedValue = localStorage.getItem(key);
            return storedValue ? JSON.parse(storedValue) : defaultValue;
        } catch (error) {
            console.error(`Помилка отримання ${key} з localStorage: ${error.message}`);
            return defaultValue;
        }
    });

    useEffect(() => {
        try {
            localStorage.setItem(key, JSON.stringify(value));
        } catch (error) {
            console.error(`Помилка збереження ${key} в localStorage: ${error.message}`);
        }
    }, [key, value]);

    return [value, setValue];
};

function App() {
    const [cart, setCart] = useLocalStorage('cart', []);
    const [favorites, setFavorites] = useLocalStorage('favorites', []);

    const addToCart = (item) => {
        setCart([...cart, item]);
    };

    const removeFromCart = (itemId) => {
        setCart(prevCart => prevCart.filter(item => item.id !== itemId));
    };

    const addToFavorites = (item) => {
        setFavorites([...favorites, item]);
    };

    const removeFromFavorites = (itemId) => {
        setFavorites(prevFavorites => prevFavorites.filter(item => item.id !== itemId));
    };

    return (
        <div>
            <Header cartCount={cart.length} favoriteCount={favorites.length} />
            <Navbar />
            <Routes>
                <Route
                    path="/"
                    element={<Home addToCart={addToCart} addToFavorites={addToFavorites} />}
                />
                <Route
                    path="/cart"
                    element={<CartPage cart={cart} removeFromCart={removeFromCart} />}
                />
                <Route
                    path="/favorites"
                    element={
                        <FavoritesPage
                            favorites={favorites} removeFromFavorites={removeFromFavorites} />}
                />
            </Routes>
        </div>
    );
}

export default App;
