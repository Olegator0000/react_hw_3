import React from 'react';
import Modal from 'react-modal';
import './modal.scss'
const ConfirmModal = ({ isOpen, onRequestClose, onConfirm }) => {
    return (
        <Modal isOpen={isOpen} onRequestClose={onRequestClose} ariaHideApp={false}  className="ConfirmModal">
            <h2>Confirm Action</h2>
            <p>Are you sure you want to remove this car from cart?</p>
            <button onClick={onConfirm}>Yes</button>
            <button onClick={onRequestClose}>No</button>
        </Modal>
    );
};

export default ConfirmModal;
