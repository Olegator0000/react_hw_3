import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ConfirmModal from '../components/modal/ConfirmModal.jsx';

const CartPage = ({ cart, removeFromCart }) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [itemToRemove, setItemToRemove] = useState(null);

    const handleRemoveClick = (itemId) => {
        setItemToRemove(itemId);
        setIsModalOpen(true);
    };

    const handleConfirmRemove = () => {
        removeFromCart(itemToRemove);
        setIsModalOpen(false);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
        setItemToRemove(null);
    };

    if (!cart || cart.length === 0) {
        return (
            <div className="cart-page">
                <h1>Cart</h1>
                <p>Your cart is empty.</p>
            </div>
        );
    }

    return (
        <div className="cart-page">
            <h1>Cart</h1>
            <div className="product-list">
                {cart.map(product => (
                    <div key={product.id} className="product-item">
                        <img src={product.imagePath} alt={product.alt} />
                        <h2>{product.name}</h2>
                        <p>{product.price}</p>
                        <button onClick={() => handleRemoveClick(product.id)}>Remove</button>
                    </div>
                ))}
            </div>
            <ConfirmModal
                isOpen={isModalOpen}
                onRequestClose={handleCloseModal}
                onConfirm={handleConfirmRemove}

            />
        </div>
    );
};

CartPage.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    removeFromCart: PropTypes.func.isRequired,
};

export default CartPage;
