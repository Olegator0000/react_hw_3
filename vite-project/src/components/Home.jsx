import React, { useState, useEffect } from 'react';
import GridImg from '../components/GridItem/GridImg';

const Home = ({ addToCart, addToFavorites }) => {
    const [products, setProducts] = useState([]);

    const fetchProducts = async () => {
        try {
            const response = await fetch('/mock.json');
            if (!response.ok) {
                throw new Error('Failed to fetch products');
            }
            const data = await response.json();
            setProducts(data);
        } catch (error) {
            console.error('Error fetching products:', error);
        }
    };

    useEffect(() => {
        fetchProducts();
    }, []);

    return (
        <div className="home">
            <h1>Home Page</h1>
            <GridImg
                items={products}
                handleBuyButtonClick={addToCart}
                handleFavoriteButtonClick={addToFavorites}
                favorites={[]}
            />
        </div>
    );
};

export default Home;
